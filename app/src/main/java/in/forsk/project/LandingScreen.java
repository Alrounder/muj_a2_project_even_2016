package in.forsk.project;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

import in.forsk.project.adapter.CustomPagerAdapter;
import in.forsk.project.adapter.LandingGridAdapter;
import in.forsk.project.wrapper.LandingGridWrapper;

public class LandingScreen extends BaseActivityWithNavigationDrawer implements LandingGridAdapter.OnItemClicklistner, ViewPager.OnPageChangeListener {
    private final static String TAG = LandingScreen.class.getSimpleName();

    private Context context;

//    private GridView mGridView;
//    private ArrayList<LandingGridWrapper> mObjList;

    private ViewPager mBannerViewPager;
    private LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;

    private FrameLayout pagerbase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_landing_screen);

        getLayoutInflater().inflate(R.layout.activity_landing_screen, baseFrame);

        context = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Manipal Jaipur");

//        mGridView = (GridView) findViewById(R.id.gridView);

//        String[] student_ids = getResources().getStringArray(R.array.student_id);
//        mObjList = new ArrayList<LandingGridWrapper>();
//        for (String id : student_ids) {
//            LandingGridWrapper mObj = new LandingGridWrapper();
//            mObj.id = id;
//
//            mObjList.add(mObj);
//        }

//        LandingGridAdapter adapter = new LandingGridAdapter(context, mObjList);
//        mGridView.setAdapter(adapter);
//        mGridView.setOnItemClickListener(this);

        pagerbase = (FrameLayout) findViewById(R.id.pagerbase);
//        pagerbase.setVisibility(View.GONE);

        mBannerViewPager = (ViewPager) findViewById(R.id.pager);
        pager_indicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);

        CustomPagerAdapter customAdapter = new CustomPagerAdapter(context,
                getResources().obtainTypedArray(R.array.dashboard_banner));
        mBannerViewPager.setAdapter(customAdapter);
        mBannerViewPager.setOnPageChangeListener(this);

        dotsCount = customAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(6, 0, 6, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));


        TypedArray dashboard_option_drawable = getResources().obtainTypedArray(R.array.dashboard_option_drawable);
        String[] dashboard_option = getResources().getStringArray(R.array.dashboard_option);

        ArrayList<LandingGridWrapper> landingGridWrapperArrayList = new ArrayList<>();
        for (int i = 0; dashboard_option.length > i; i++) {
            LandingGridWrapper landingGridWrapper = new LandingGridWrapper();
            landingGridWrapper.title = dashboard_option[i];
            landingGridWrapper.drawable = dashboard_option_drawable.getResourceId(i, 0);

            landingGridWrapperArrayList.add(landingGridWrapper);
        }

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 3, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        LandingGridAdapter adapter = new LandingGridAdapter(landingGridWrapperArrayList);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClicklistner(this);
    }
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));
        }

        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


//    @Override
//    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
////        Toast.makeText(context, mObjList.get(position).id, Toast.LENGTH_SHORT).show();
//
////        String clicked_id = mObjList.get(position).id;
//        String clicked_id = "";
//
//        if (clicked_id.equalsIgnoreCase("saurabh")) {
//
//        } else if (clicked_id.equalsIgnoreCase("lokesh")) {
//            Intent intent = new Intent(LandingScreen.this, Lokesh_Sample_Activity.class);
//            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("rohit")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("159101123")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("159102014")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("159106089")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("159102103")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("159105058")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("159102101")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111148")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105460")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105266")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105542")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105280")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105232")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105554")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105196")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105476")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105244")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105532")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105252")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149106066")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105604")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105534")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111150")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105584")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105340")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111654")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111188")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111336")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111142")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111574")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111580")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111198")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111759")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111522")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111398")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111648")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111710")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111680")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111534")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111494")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111757")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        }
//
//        //#################A2##########################
//        else if (clicked_id.equalsIgnoreCase("149111130")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111090")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105618")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149107130")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149107682")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105432")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105556")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105086")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105314")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105434")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105064")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105048")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105562")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105540")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105394")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105032")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105392")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105576")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105005")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105044")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105544")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105550")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105586")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105142")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105008")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149105052")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111670")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111588")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111748")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111328")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111660")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111664")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("120402053")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149106742")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111758")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149104532")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149106728")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("139105005")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("139105346")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        } else if (clicked_id.equalsIgnoreCase("149111236")) {
////            Intent intent = new Intent(LandingScreen.this, Put_Your_Activity_Name_Here.class);
////            startActivity(intent);
//        }
//    }

    @Override
    public void onItemClick(View v, int position) {
        switch (position){
            case 0:
                break;
        }
    }
}
